'use strict'

ItemView = require('views/base/item')

module.exports = class QueryResultsItemView extends ItemView
  className: 'query-results-item-view list-group-item'
  template: require('./template')

  updateSentiment: (result) ->
    @$('.spinner').addClass 'hide'
    if result?
      @$el.css 'color',
      if result.sentiment is 'positive' then 'green' else 'red'

