'use strict'

CollectionView = require('views/base/collection')
QueryResultsItemView = require('./item')
SentimentService = require 'services/sentiment'

module.exports = class QueryResultsView extends CollectionView
  className: 'query-results-view'
  template: require('./template')
  itemView: QueryResultsItemView
  optionNames: CollectionView::optionNames.concat ['query']

  listSelector: '.query-results-list'

  getTemplateData: ->
    $.extend {}, super, query:@query

  initItemView: (model) ->
    view = super
    SentimentService.sendRequest(model.get 'text')
      .done (result) -> view.updateSentiment($.parseJSON result)
    view

