'use strict'

View = require('views/base/view')

module.exports = class PageView extends View
  className: 'page-view container'
  template: require('./template')

  regions :
    'query-input' : '.query-input-container'
    'query-result' : '.query-result-container'
