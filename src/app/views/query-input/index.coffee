'use strict'

View = require('views/base/view')

module.exports = class QueryInputView extends View
  className: 'query-input-view'
  template: require('./template')

  attach: ->
    super
    @$('.query-input').keyup (e) => if e.which is 13 then @submitQuery()
    @$('button.submit').click @submitQuery

  submitQuery: =>
    $input =  @$('.query-input')
    val = $input.val()
    $input.val('')
    @trigger 'userinput', val

