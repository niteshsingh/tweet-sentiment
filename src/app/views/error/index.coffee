'use strict'

View = require('views/base/view')

module.exports = class ErrorView extends View
  className: 'error-view'
  template: require('./template')
