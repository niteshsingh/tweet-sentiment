'use strict'

Controller       = require('controllers/base/controller')
PageView         = require 'views/page'
QueryInputView   = require 'views/query-input'
QueryResultsView = require 'views/query-results'
ErrorView        = require 'views/error'
BaseModel        = require 'models/base/model'
Tweets           = require 'models/tweets'

Mediator = Chaplin.mediator

module.exports = class AppController extends Controller

  beforeAction: ->
    Mediator.twitterService.authPopup()

  index: ->
    @view = new PageView
      container : 'body' 

    queryInputView = new QueryInputView
      region : 'query-input' 
      containerMethod: 'html'

    @listenTo queryInputView, 'userinput', @fetchTweets

  # fetch tweets for query
  fetchTweets: (query) ->
    service = Mediator.twitterService    
    service.getCachedResult().done (result) =>
      gettingTweets = result.get(service.getURL query)
      gettingTweets.done (response) => @showResults(query)(response)
      gettingTweets.fail => @showResults.apply @, arguments


  showResults: (query) => (response) ->
    if response? then searchedTweets = new Tweets response.statuses
    new QueryResultsView
      region : 'query-result'
      containerMethod : 'html'
      collection : searchedTweets
      query : query
    

  showError: (args...) =>

    console.log args
    new ErrorView
      region : 'query-result'
      containerMethod: 'html'
      model :  new BaseModel