'use strict'

Collection = require('models/base/collection')
Tweet = require('./tweet')

module.exports = class Tweets extends Collection
  model: Tweet
