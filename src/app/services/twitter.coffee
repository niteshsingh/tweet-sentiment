
module.exports = class TwitterService

  @searchURL: '1.1/search/tweets.json'
  
  defaults:
    lang: 'en'
    result_type: 'popular'
    count: '10'

  initialize: (public_key) -> OAuth.initialize public_key
  
  authPopup: (provider = 'twitter') -> @_promise = OAuth.popup provider

  getCachedResult: -> @_promise

  getURL: (query, options) ->
    options = $.extend {}, @defaults, options, q : encodeURI(query)
    "#{@constructor.searchURL}?#{$.param options, true}"