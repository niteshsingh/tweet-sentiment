'use strict'

SentimentConfig = require 'configs/tweet-sentiment'

module.exports = class SentimentService

  @sendRequest: (text, key = SentimentConfig.public_key) ->
    $.ajax
      type : 'GET'
      crossDomain : yes

      beforeSend: (xhr) ->
        mashape = SentimentConfig.mashape
        xhr.setRequestHeader mashape.header, mashape.headerValue

      url : SentimentConfig.api_url
      data :
        key : key
        text : text

      
  
  
