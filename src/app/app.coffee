'use strict'

routes = require('routes')
utils = require('utils')

OAuthConfig = require 'configs/oauth-io'
TwitterService = require 'services/twitter'

defaultOptions = {routes, controllerSuffix: ''}

Mediator = Chaplin.mediator

module.exports = class Application extends Chaplin.Application

  constructor: (options) ->
    super utils.extend({}, defaultOptions, options)

  initMediator: ->
    mediator = _.extend Mediator, twitterService : new TwitterService
    mediator.seal()

  start: ->
    Mediator.twitterService.initialize OAuthConfig.public_key
    super
