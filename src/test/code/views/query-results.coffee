'use strict'

QueryResultsView = require('views/query-results')
QueryResultsItemView = require('views/query-results/item')

describe 'QueryResultsView', ->

  beforeEach ->
    @queryResultsView = new QueryResultsView
    @queryResultsItemView = new QueryResultsItemView



  afterEach ->
    @queryResultsView.dispose()
    @queryResultsItemView.dispose()
