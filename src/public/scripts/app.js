(function(/*! Brunch !*/) {
  'use strict';

  var globals = typeof window !== 'undefined' ? window : global;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};

  var has = function(object, name) {
    return ({}).hasOwnProperty.call(object, name);
  };

  var expand = function(root, name) {
    var results = [], parts, part;
    if (/^\.\.?(\/|$)/.test(name)) {
      parts = [root, name].join('/').split('/');
    } else {
      parts = name.split('/');
    }
    for (var i = 0, length = parts.length; i < length; i++) {
      part = parts[i];
      if (part === '..') {
        results.pop();
      } else if (part !== '.' && part !== '') {
        results.push(part);
      }
    }
    return results.join('/');
  };

  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function(name) {
      var dir = dirname(path);
      var absolute = expand(dir, name);
      return globals.require(absolute, path);
    };
  };

  var initModule = function(name, definition) {
    var module = {id: name, exports: {}};
    cache[name] = module;
    definition(module.exports, localRequire(name), module);
    return module.exports;
  };

  var require = function(name, loaderPath) {
    var path = expand(name, '.');
    if (loaderPath == null) loaderPath = '/';

    if (has(cache, path)) return cache[path].exports;
    if (has(modules, path)) return initModule(path, modules[path]);

    var dirIndex = expand(path, './index');
    if (has(cache, dirIndex)) return cache[dirIndex].exports;
    if (has(modules, dirIndex)) return initModule(dirIndex, modules[dirIndex]);

    throw new Error('Cannot find module "' + name + '" from '+ '"' + loaderPath + '"');
  };

  var define = function(bundle, fn) {
    if (typeof bundle === 'object') {
      for (var key in bundle) {
        if (has(bundle, key)) {
          modules[key] = bundle[key];
        }
      }
    } else {
      modules[bundle] = fn;
    }
  };

  var list = function() {
    var result = [];
    for (var item in modules) {
      if (has(modules, item)) {
        result.push(item);
      }
    }
    return result;
  };

  globals.require = require;
  globals.require.define = define;
  globals.require.register = define;
  globals.require.list = list;
  globals.require.brunch = true;
})();
require.register("app", function(exports, require, module) {
'use strict';
var Application, Mediator, OAuthConfig, TwitterService, defaultOptions, routes, utils,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

routes = require('routes');

utils = require('utils');

OAuthConfig = require('configs/oauth-io');

TwitterService = require('services/twitter');

defaultOptions = {
  routes: routes,
  controllerSuffix: ''
};

Mediator = Chaplin.mediator;

module.exports = Application = (function(_super) {
  __extends(Application, _super);

  function Application(options) {
    Application.__super__.constructor.call(this, utils.extend({}, defaultOptions, options));
  }

  Application.prototype.initMediator = function() {
    var mediator;
    mediator = _.extend(Mediator, {
      twitterService: new TwitterService
    });
    return mediator.seal();
  };

  Application.prototype.start = function() {
    Mediator.twitterService.initialize(OAuthConfig.public_key);
    return Application.__super__.start.apply(this, arguments);
  };

  return Application;

})(Chaplin.Application);
});

;require.register("configs/oauth-io", function(exports, require, module) {
'use strict';
var Config;

module.exports = Config = {
  public_key: 'bSLT39yx80bH56V62RGa084QEIw'
};
});

;require.register("configs/tweet-sentiment", function(exports, require, module) {
'use strict';
var Config;

module.exports = Config = {
  mashape: {
    header: 'X-Mashape-Key',
    headerValue: 'Jn9B0EodJvmshCAafxrIoOvVugN1p1pYKNVjsnQunJRmq4z9li'
  },
  api_url: 'https://jamiembrown-tweet-sentiment-analysis.p.mashape.com/api/',
  public_key: '7e91b407037da5072baf2c8ff692d1988ed27167'
};
});

;require.register("controllers/app", function(exports, require, module) {
'use strict';
var AppController, BaseModel, Controller, ErrorView, Mediator, PageView, QueryInputView, QueryResultsView, Tweets,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  __slice = [].slice;

Controller = require('controllers/base/controller');

PageView = require('views/page');

QueryInputView = require('views/query-input');

QueryResultsView = require('views/query-results');

ErrorView = require('views/error');

BaseModel = require('models/base/model');

Tweets = require('models/tweets');

Mediator = Chaplin.mediator;

module.exports = AppController = (function(_super) {
  __extends(AppController, _super);

  function AppController() {
    this.showError = __bind(this.showError, this);
    this.showResults = __bind(this.showResults, this);
    return AppController.__super__.constructor.apply(this, arguments);
  }

  AppController.prototype.beforeAction = function() {
    return Mediator.twitterService.authPopup();
  };

  AppController.prototype.index = function() {
    var queryInputView;
    this.view = new PageView({
      container: 'body'
    });
    queryInputView = new QueryInputView({
      region: 'query-input',
      containerMethod: 'html'
    });
    return this.listenTo(queryInputView, 'userinput', this.fetchTweets);
  };

  AppController.prototype.fetchTweets = function(query) {
    var service;
    service = Mediator.twitterService;
    return service.getCachedResult().done((function(_this) {
      return function(result) {
        var gettingTweets;
        gettingTweets = result.get(service.getURL(query));
        gettingTweets.done(function(response) {
          return _this.showResults(query)(response);
        });
        return gettingTweets.fail(function() {
          return _this.showResults.apply(_this, arguments);
        });
      };
    })(this));
  };

  AppController.prototype.showResults = function(query) {
    return function(response) {
      var searchedTweets;
      if (response != null) {
        searchedTweets = new Tweets(response.statuses);
      }
      return new QueryResultsView({
        region: 'query-result',
        containerMethod: 'html',
        collection: searchedTweets,
        query: query
      });
    };
  };

  AppController.prototype.showError = function() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    console.log(args);
    return new ErrorView({
      region: 'query-result',
      containerMethod: 'html',
      model: new BaseModel
    });
  };

  return AppController;

})(Controller);
});

;require.register("controllers/base/controller", function(exports, require, module) {
'use strict';
var Controller,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

module.exports = Controller = (function(_super) {
  __extends(Controller, _super);

  function Controller() {
    return Controller.__super__.constructor.apply(this, arguments);
  }

  return Controller;

})(Chaplin.Controller);
});

;require.register("initialize", function(exports, require, module) {
'use strict';
var initialize;

initialize = function() {
  var App;
  if (!Backbone.Deferred && window.Davy) {
    Backbone.Deferred = function() {
      return new Davy;
    };
  }
  if (window.rivets) {
    rivets.adapters[':'] = {
      subscribe: function(obj, keypath, callback) {
        return obj.on("change:" + keypath, callback);
      },
      unsubscribe: function(obj, keypath, callback) {
        return obj.off("change:" + keypath, callback);
      },
      read: function(obj, keypath) {
        return obj.get(keypath);
      },
      publish: function(obj, keypath, value) {
        return obj.set(keypath, value);
      }
    };
  }
  App = require('app');
  return new App({
    pushState: false
  });
};

if (window.$) {
  $(document).ready(initialize);
} else {
  document.addEventListener('DOMContentLoaded', initialize);
}
});

;require.register("models/base/collection", function(exports, require, module) {
'use strict';
var Collection, Model,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Model = require('./model');

module.exports = Collection = (function(_super) {
  __extends(Collection, _super);

  function Collection() {
    return Collection.__super__.constructor.apply(this, arguments);
  }

  Collection.prototype.model = Model;

  return Collection;

})(Chaplin.Collection);
});

;require.register("models/base/model", function(exports, require, module) {
'use strict';
var Model,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

module.exports = Model = (function(_super) {
  __extends(Model, _super);

  function Model() {
    return Model.__super__.constructor.apply(this, arguments);
  }

  return Model;

})(Chaplin.Model);
});

;require.register("models/tweet", function(exports, require, module) {
'use strict';
var Model, Tweet,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Model = require('models/base/model');

module.exports = Tweet = (function(_super) {
  __extends(Tweet, _super);

  function Tweet() {
    return Tweet.__super__.constructor.apply(this, arguments);
  }

  return Tweet;

})(Model);
});

;require.register("models/tweets", function(exports, require, module) {
'use strict';
var Collection, Tweet, Tweets,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Collection = require('models/base/collection');

Tweet = require('./tweet');

module.exports = Tweets = (function(_super) {
  __extends(Tweets, _super);

  function Tweets() {
    return Tweets.__super__.constructor.apply(this, arguments);
  }

  Tweets.prototype.model = Tweet;

  return Tweets;

})(Collection);
});

;require.register("routes", function(exports, require, module) {
'use strict';
module.exports = function(match) {
  return match('', 'app#index');
};
});

;require.register("services/sentiment", function(exports, require, module) {
'use strict';
var SentimentConfig, SentimentService;

SentimentConfig = require('configs/tweet-sentiment');

module.exports = SentimentService = (function() {
  function SentimentService() {}

  SentimentService.sendRequest = function(text, key) {
    if (key == null) {
      key = SentimentConfig.public_key;
    }
    return $.ajax({
      type: 'GET',
      crossDomain: true,
      beforeSend: function(xhr) {
        var mashape;
        mashape = SentimentConfig.mashape;
        return xhr.setRequestHeader(mashape.header, mashape.headerValue);
      },
      url: SentimentConfig.api_url,
      data: {
        key: key,
        text: text
      }
    });
  };

  return SentimentService;

})();
});

;require.register("services/twitter", function(exports, require, module) {
var TwitterService;

module.exports = TwitterService = (function() {
  function TwitterService() {}

  TwitterService.searchURL = '1.1/search/tweets.json';

  TwitterService.prototype.defaults = {
    lang: 'en',
    result_type: 'popular',
    count: '10'
  };

  TwitterService.prototype.initialize = function(public_key) {
    return OAuth.initialize(public_key);
  };

  TwitterService.prototype.authPopup = function(provider) {
    if (provider == null) {
      provider = 'twitter';
    }
    return this._promise = OAuth.popup(provider);
  };

  TwitterService.prototype.getCachedResult = function() {
    return this._promise;
  };

  TwitterService.prototype.getURL = function(query, options) {
    options = $.extend({}, this.defaults, options, {
      q: encodeURI(query)
    });
    return "" + this.constructor.searchURL + "?" + ($.param(options, true));
  };

  return TwitterService;

})();
});

;require.register("utils", function(exports, require, module) {
'use strict';
var utils;

module.exports = utils = Chaplin.utils.beget(Chaplin.utils);

utils.extend = window._ ? _.extend : Backbone.utils.extend;
});

;require.register("views/base/collection", function(exports, require, module) {
'use strict';
var CollectionView, View,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

View = require('./view');

module.exports = CollectionView = (function(_super) {
  __extends(CollectionView, _super);

  function CollectionView() {
    return CollectionView.__super__.constructor.apply(this, arguments);
  }

  CollectionView.prototype.getTemplateFunction = View.prototype.getTemplateFunction;

  return CollectionView;

})(Chaplin.CollectionView);
});

;require.register("views/base/item", function(exports, require, module) {
'use strict';
var ItemView, View,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

View = require('./view');

module.exports = ItemView = (function(_super) {
  __extends(ItemView, _super);

  function ItemView() {
    return ItemView.__super__.constructor.apply(this, arguments);
  }

  return ItemView;

})(View);
});

;require.register("views/base/view", function(exports, require, module) {
'use strict';
var View,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

module.exports = View = (function(_super) {
  __extends(View, _super);

  function View() {
    return View.__super__.constructor.apply(this, arguments);
  }

  View.prototype.autoRender = true;

  View.prototype.getTemplateFunction = function() {
    return this.template;
  };

  View.prototype.render = function() {
    View.__super__.render.apply(this, arguments);
    if (!(this.model && window.rivets)) {
      return;
    }
    if (this._rivets) {
      return this._rivets.build();
    } else {
      return this._rivets = rivets.bind(this.el, {
        model: this.model
      });
    }
  };

  View.prototype.dispose = function() {
    var _ref;
    if ((_ref = this._rivets) != null) {
      _ref.unbind();
    }
    delete this._rivets;
    return View.__super__.dispose.apply(this, arguments);
  };

  return View;

})(Chaplin.View);
});

;require.register("views/error/index", function(exports, require, module) {
'use strict';
var ErrorView, View,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

View = require('views/base/view');

module.exports = ErrorView = (function(_super) {
  __extends(ErrorView, _super);

  function ErrorView() {
    return ErrorView.__super__.constructor.apply(this, arguments);
  }

  ErrorView.prototype.className = 'error-view';

  ErrorView.prototype.template = require('./template');

  return ErrorView;

})(View);
});

;require.register("views/error/template", function(exports, require, module) {
var __templateData = function (__obj) {
  if (!__obj) __obj = {};
  var __out = [], __capture = function(callback) {
    var out = __out, result;
    __out = [];
    callback.call(this);
    result = __out.join('');
    __out = out;
    return __safe(result);
  }, __sanitize = function(value) {
    if (value && value.ecoSafe) {
      return value;
    } else if (typeof value !== 'undefined' && value != null) {
      return __escape(value);
    } else {
      return '';
    }
  }, __safe, __objSafe = __obj.safe, __escape = __obj.escape;
  __safe = __obj.safe = function(value) {
    if (value && value.ecoSafe) {
      return value;
    } else {
      if (!(typeof value !== 'undefined' && value != null)) value = '';
      var result = new String(value);
      result.ecoSafe = true;
      return result;
    }
  };
  if (!__escape) {
    __escape = __obj.escape = function(value) {
      return ('' + value)
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;');
    };
  }
  (function() {
    (function() {
      __out.push('<p class="text-danger">\n\tFailed to fetch tweets. ');
    
      __out.push(__sanitize(this.error && this.error));
    
      __out.push('\n</p>');
    
    }).call(this);
    
  }).call(__obj);
  __obj.safe = __objSafe, __obj.escape = __escape;
  return __out.join('');
};
if (typeof define === 'function' && define.amd) {
  define([], function() {
    return __templateData;
  });
} else if (typeof module === 'object' && module && module.exports) {
  module.exports = __templateData;
} else {
  __templateData;
}
});

;require.register("views/page/index", function(exports, require, module) {
'use strict';
var PageView, View,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

View = require('views/base/view');

module.exports = PageView = (function(_super) {
  __extends(PageView, _super);

  function PageView() {
    return PageView.__super__.constructor.apply(this, arguments);
  }

  PageView.prototype.className = 'page-view container';

  PageView.prototype.template = require('./template');

  PageView.prototype.regions = {
    'query-input': '.query-input-container',
    'query-result': '.query-result-container'
  };

  return PageView;

})(View);
});

;require.register("views/page/template", function(exports, require, module) {
var __templateData = function (__obj) {
  if (!__obj) __obj = {};
  var __out = [], __capture = function(callback) {
    var out = __out, result;
    __out = [];
    callback.call(this);
    result = __out.join('');
    __out = out;
    return __safe(result);
  }, __sanitize = function(value) {
    if (value && value.ecoSafe) {
      return value;
    } else if (typeof value !== 'undefined' && value != null) {
      return __escape(value);
    } else {
      return '';
    }
  }, __safe, __objSafe = __obj.safe, __escape = __obj.escape;
  __safe = __obj.safe = function(value) {
    if (value && value.ecoSafe) {
      return value;
    } else {
      if (!(typeof value !== 'undefined' && value != null)) value = '';
      var result = new String(value);
      result.ecoSafe = true;
      return result;
    }
  };
  if (!__escape) {
    __escape = __obj.escape = function(value) {
      return ('' + value)
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;');
    };
  }
  (function() {
    (function() {
      __out.push('<div class="query-input-container"></div>\n<div class="query-result-container"></div>');
    
    }).call(this);
    
  }).call(__obj);
  __obj.safe = __objSafe, __obj.escape = __escape;
  return __out.join('');
};
if (typeof define === 'function' && define.amd) {
  define([], function() {
    return __templateData;
  });
} else if (typeof module === 'object' && module && module.exports) {
  module.exports = __templateData;
} else {
  __templateData;
}
});

;require.register("views/query-input/index", function(exports, require, module) {
'use strict';
var QueryInputView, View,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

View = require('views/base/view');

module.exports = QueryInputView = (function(_super) {
  __extends(QueryInputView, _super);

  function QueryInputView() {
    this.submitQuery = __bind(this.submitQuery, this);
    return QueryInputView.__super__.constructor.apply(this, arguments);
  }

  QueryInputView.prototype.className = 'query-input-view';

  QueryInputView.prototype.template = require('./template');

  QueryInputView.prototype.attach = function() {
    QueryInputView.__super__.attach.apply(this, arguments);
    this.$('.query-input').keyup((function(_this) {
      return function(e) {
        if (e.which === 13) {
          return _this.submitQuery();
        }
      };
    })(this));
    return this.$('button.submit').click(this.submitQuery);
  };

  QueryInputView.prototype.submitQuery = function() {
    var $input, val;
    $input = this.$('.query-input');
    val = $input.val();
    $input.val('');
    return this.trigger('userinput', val);
  };

  return QueryInputView;

})(View);
});

;require.register("views/query-input/template", function(exports, require, module) {
var __templateData = function (__obj) {
  if (!__obj) __obj = {};
  var __out = [], __capture = function(callback) {
    var out = __out, result;
    __out = [];
    callback.call(this);
    result = __out.join('');
    __out = out;
    return __safe(result);
  }, __sanitize = function(value) {
    if (value && value.ecoSafe) {
      return value;
    } else if (typeof value !== 'undefined' && value != null) {
      return __escape(value);
    } else {
      return '';
    }
  }, __safe, __objSafe = __obj.safe, __escape = __obj.escape;
  __safe = __obj.safe = function(value) {
    if (value && value.ecoSafe) {
      return value;
    } else {
      if (!(typeof value !== 'undefined' && value != null)) value = '';
      var result = new String(value);
      result.ecoSafe = true;
      return result;
    }
  };
  if (!__escape) {
    __escape = __obj.escape = function(value) {
      return ('' + value)
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;');
    };
  }
  (function() {
    (function() {
      __out.push('<div class="row">\n\t<div class="h5 col-xs-12">\n\t\tSearch For :\n\t</div>\n</div>\n\n<div class="row">\n\t<div class="col-xs-12">\n\t\t<div class="input-group">\n\t\t\t<input type="text" class="form-control query-input" autocomplete="off" autofocus placeholder="search for most popular tweets">\n\t\t\t<span class="input-group-btn">\n\t\t\t\t<button class="btn btn-default submit" type="button">Submit</button>\n\t\t\t</span>\n\t\t</div>\n\t</div>\n</div>');
    
    }).call(this);
    
  }).call(__obj);
  __obj.safe = __objSafe, __obj.escape = __escape;
  return __out.join('');
};
if (typeof define === 'function' && define.amd) {
  define([], function() {
    return __templateData;
  });
} else if (typeof module === 'object' && module && module.exports) {
  module.exports = __templateData;
} else {
  __templateData;
}
});

;require.register("views/query-results/index", function(exports, require, module) {
'use strict';
var CollectionView, QueryResultsItemView, QueryResultsView, SentimentService,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

CollectionView = require('views/base/collection');

QueryResultsItemView = require('./item');

SentimentService = require('services/sentiment');

module.exports = QueryResultsView = (function(_super) {
  __extends(QueryResultsView, _super);

  function QueryResultsView() {
    return QueryResultsView.__super__.constructor.apply(this, arguments);
  }

  QueryResultsView.prototype.className = 'query-results-view';

  QueryResultsView.prototype.template = require('./template');

  QueryResultsView.prototype.itemView = QueryResultsItemView;

  QueryResultsView.prototype.optionNames = CollectionView.prototype.optionNames.concat(['query']);

  QueryResultsView.prototype.listSelector = '.query-results-list';

  QueryResultsView.prototype.getTemplateData = function() {
    return $.extend({}, QueryResultsView.__super__.getTemplateData.apply(this, arguments), {
      query: this.query
    });
  };

  QueryResultsView.prototype.initItemView = function(model) {
    var view;
    view = QueryResultsView.__super__.initItemView.apply(this, arguments);
    SentimentService.sendRequest(model.get('text')).done(function(result) {
      return view.updateSentiment($.parseJSON(result));
    });
    return view;
  };

  return QueryResultsView;

})(CollectionView);
});

;require.register("views/query-results/item/index", function(exports, require, module) {
'use strict';
var ItemView, QueryResultsItemView,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

ItemView = require('views/base/item');

module.exports = QueryResultsItemView = (function(_super) {
  __extends(QueryResultsItemView, _super);

  function QueryResultsItemView() {
    return QueryResultsItemView.__super__.constructor.apply(this, arguments);
  }

  QueryResultsItemView.prototype.className = 'query-results-item-view list-group-item';

  QueryResultsItemView.prototype.template = require('./template');

  QueryResultsItemView.prototype.updateSentiment = function(result) {
    this.$('.spinner').addClass('hide');
    if (result != null) {
      return this.$el.css('color', result.sentiment === 'positive' ? 'green' : 'red');
    }
  };

  return QueryResultsItemView;

})(ItemView);
});

;require.register("views/query-results/item/template", function(exports, require, module) {
var __templateData = function (__obj) {
  if (!__obj) __obj = {};
  var __out = [], __capture = function(callback) {
    var out = __out, result;
    __out = [];
    callback.call(this);
    result = __out.join('');
    __out = out;
    return __safe(result);
  }, __sanitize = function(value) {
    if (value && value.ecoSafe) {
      return value;
    } else if (typeof value !== 'undefined' && value != null) {
      return __escape(value);
    } else {
      return '';
    }
  }, __safe, __objSafe = __obj.safe, __escape = __obj.escape;
  __safe = __obj.safe = function(value) {
    if (value && value.ecoSafe) {
      return value;
    } else {
      if (!(typeof value !== 'undefined' && value != null)) value = '';
      var result = new String(value);
      result.ecoSafe = true;
      return result;
    }
  };
  if (!__escape) {
    __escape = __obj.escape = function(value) {
      return ('' + value)
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;');
    };
  }
  (function() {
    (function() {
      __out.push(__sanitize(this.text));
    
      __out.push('\n<span class="spinner pull-right">\n\t<img src="./images/spinner.gif">\n</span>');
    
    }).call(this);
    
  }).call(__obj);
  __obj.safe = __objSafe, __obj.escape = __escape;
  return __out.join('');
};
if (typeof define === 'function' && define.amd) {
  define([], function() {
    return __templateData;
  });
} else if (typeof module === 'object' && module && module.exports) {
  module.exports = __templateData;
} else {
  __templateData;
}
});

;require.register("views/query-results/template", function(exports, require, module) {
var __templateData = function (__obj) {
  if (!__obj) __obj = {};
  var __out = [], __capture = function(callback) {
    var out = __out, result;
    __out = [];
    callback.call(this);
    result = __out.join('');
    __out = out;
    return __safe(result);
  }, __sanitize = function(value) {
    if (value && value.ecoSafe) {
      return value;
    } else if (typeof value !== 'undefined' && value != null) {
      return __escape(value);
    } else {
      return '';
    }
  }, __safe, __objSafe = __obj.safe, __escape = __obj.escape;
  __safe = __obj.safe = function(value) {
    if (value && value.ecoSafe) {
      return value;
    } else {
      if (!(typeof value !== 'undefined' && value != null)) value = '';
      var result = new String(value);
      result.ecoSafe = true;
      return result;
    }
  };
  if (!__escape) {
    __escape = __obj.escape = function(value) {
      return ('' + value)
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;');
    };
  }
  (function() {
    (function() {
      __out.push('<p>\n\t<div class="row">\n\t\t<div class="h5 col-xs-12">\n\t\t\tTwitter Results: <strong>');
    
      __out.push(__sanitize(this.query));
    
      __out.push(' </strong>\n\t\t</div>\n\t</div>\n\t<div class="row">\n\t\t<div class="col-xs-12">\n\t\t\t<div class="query-results-list list-group"></div>\n\t\t</div>\n\t</div>\n\n\t<div class="row">\n\t\t<div class="legend-container col-xs-12">\n\t\t\t<p><strong>Legend</strong></p>\n\t\t\t<p class="positive-tweets text-success"> Positive tweets in green.</p>\t\n\t\t\t<p class="negative-tweets text-danger"> Negative tweets in red.</p>\t\n\t\t</div>\n\t</div>\n</p>');
    
    }).call(this);
    
  }).call(__obj);
  __obj.safe = __objSafe, __obj.escape = __escape;
  return __out.join('');
};
if (typeof define === 'function' && define.amd) {
  define([], function() {
    return __templateData;
  });
} else if (typeof module === 'object' && module && module.exports) {
  module.exports = __templateData;
} else {
  __templateData;
}
});

;
//# sourceMappingURL=app.js.map